<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;
use Netzwolke\Manager\Model\Resources\CollectionExtension;

/**
 * Class BaseModel
 * @package netzwolke\Model
 */
class BaseModel extends Model implements ModelInterface
{
    public function newCollection(array $models = []): CollectionExtension
    {
        return new CollectionExtension($models);
    }

    /**
     * @return bool
     */
    public static function hasTable(): bool
    {
        return Manager::schema()->hasTable((new static())->getTable());
    }
}
