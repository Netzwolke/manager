<?php /** @noinspection ALL */
/** @noinspection ALL */

/** @noinspection ALL */

namespace Netzwolke\Manager\Model;

/**
 * Class UserLog
 * @package netzwolke\Model
 */
class UserLog extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'usersLogs';
    /**
     * @var array
     */
    protected $guarded = [];

    public function user()
    {
        $this->belongsTo(User::class, 'userId', 'id');
    }
}
