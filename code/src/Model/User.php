<?php /** @noinspection ALL */

namespace Netzwolke\Manager\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Netzwolke\Manager\Model\Pivot\GrUs;
use Netzwolke\Manager\Model\Pivot\UsRo;

/**
 * Class User
 * @package netzwolke\Model
 */
class User extends BaseModel
{
    /**
     * @var string
     */
    protected $connection = 'default';
    /**
     * @var string
     */
    protected $table = 'users';
    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * @return BelongsTo
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'usRo_pivot', 'userId', 'roleId')
            ->using(UsRo::class)
            ->withTimestamps();
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'grUs_pivot', 'userId', 'groupId')
            ->using(GrUs::class)
            ->withTimestamps();
    }
}
