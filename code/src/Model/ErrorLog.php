<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model;

/**
 * Class ErrorLog
 * @package Netzwolke\Manager\Model
 */
class ErrorLog extends BaseModel
{
    /**
     * @var string
     */
    protected $connection = 'error';
    /**
     * @var string
     */
    protected $table = 'errorLogs';
}
