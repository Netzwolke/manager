<?php /** @noinspection ALL */

namespace Netzwolke\Manager\Model;

/**
 * Class HttpRequest
 * @package netzwolke\Model
 */
class HttpRequest extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'httpRequests';
    protected $guarded = [];
}
