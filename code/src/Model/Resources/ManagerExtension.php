<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Resources;

use Illuminate\Database\Capsule\Manager;

/**
 * Class ManagerExtension
 * @package Netzwolke\Manager\Model\Resources
 */
class ManagerExtension extends Manager
{
    public function __construct(
        private PaginationInterface $pagination
    ) {
        parent::__construct();
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        return $this->pagination;
    }

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public static function getInstance(): self
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }
        return false;
    }
}
