<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Resources;

use Slim\Interfaces\RouteInterface;
use Slim\Interfaces\RouteParserInterface;

/**
 * Class Pagination
 * @package Netzwolke\Manager\Model\Resources
 */
interface PaginationInterface
{
    /**
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void;

    /**
     * @return int
     */
    public function getMaxPages(): int;

    /**
     * @param int $maxPages
     */
    public function setMaxPages(int $maxPages): void;

    /**
     * @return bool
     */
    public function isPaginated(): bool;

    /**
     * @param bool $isset
     */
    public function setPaginated(bool $isset): void;

    /**
     * @param RouteInterface $route
     */
    public function setRoute(RouteInterface $route): void;

    /**
     * @return ?RouteInterface
     */
    public function getRoute(): ?RouteInterface;

    /**
     * @return RouteParserInterface
     */
    public function getRouteParser(): RouteParserInterface;

    /**
     * @param array $queryParams
     */
    public function setQueryParams(array $queryParams = []): void;

    /**
     * @return array
     */
    public function getQueryParams(): array;
}
