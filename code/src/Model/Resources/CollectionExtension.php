<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Resources;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class CollectionExtension
 * @package Netzwolke\Manager\Model\Resources
 */
class CollectionExtension extends Collection
{
    /**
     * @param int $entries
     * @return mixed|void
     */
    public function paginate(int $entries = 10)
    {
        if ($this->isEmpty()) {
            return;
        }
        $collection = $this;
        $chunk = $this->chunk($entries);
        if ($pagination = ManagerExtension::getInstance()->getPagination()) {
            $page = $pagination->getCurrentPage();
            if ($page > $chunk->count()) {
                $page = $chunk->count() - 1;
            } elseif ($page < 1) {
                $page = 0;
            } else {
                $page--;
            }
            if ($chunk->count() > 1) {
                $pagination->setPaginated(true);
            }
            $pagination->setMaxPages($chunk->count());
            $collection = $chunk->get($page);
        }
        return $collection;
    }
}
