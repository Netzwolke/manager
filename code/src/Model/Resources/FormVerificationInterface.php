<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Resources;

use Netzwolke\Manager\Model\ModelInterface;

/**
 * Class FormVerification
 * @package Netzwolke\Manager\Model\Resources
 */
interface FormVerificationInterface
{
    /**
     * @return string
     */
    public function getFormId(): string;

    /**
     * @return ModelInterface
     */
    public function getModel(): ModelInterface;

    /**
     * @param ModelInterface $model
     */
    public function setModel(ModelInterface $model): void;

    /**
     * @param string $formId
     */
    public function setFormId(string $formId): void;

    /**
     * @param FormVerificationInterface $verification
     * @return mixed
     */
    public function fromObject(FormVerificationInterface $verification): void;
}
