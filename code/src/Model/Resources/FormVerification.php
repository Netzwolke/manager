<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Resources;

use Netzwolke\Manager\Model\ModelInterface;

/**
 * Class FormVerification
 * @package Netzwolke\Manager\Model\Resources
 */
class FormVerification implements FormVerificationInterface
{
    public function __construct(
        private ModelInterface $model,
        private string $formId = '',
    ) {
        $this->formId = uniqid("ID");
    }

    public function getFormId(): string
    {
        return $this->formId;
    }

    public function getModel(): ModelInterface
    {
        return $this->model;
    }

    public function setModel(ModelInterface $model): void
    {
        $this->model = $model;
    }

    public function setFormId(string $formId): void
    {
        $this->formId = $formId;
    }

    /**
     * @param FormVerificationInterface $verification
     */
    public function fromObject(FormVerificationInterface $verification): void
    {
        $this->setFormId($verification->getFormId());
        $this->setModel($verification->getModel());
    }
}
