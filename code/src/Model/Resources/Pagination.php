<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Resources;

use Slim\Interfaces\RouteInterface;
use Slim\Interfaces\RouteParserInterface;

/**
 * Class Pagination
 * @package Netzwolke\Manager\Model\Resources
 */
class Pagination implements PaginationInterface
{
    /**
     * @var RouteInterface|null
     */
    private ?RouteInterface $route;
    /**
     * @var array
     */
    private array $queryParams;

    /**
     * Pagination constructor.
     * @param int $currentPage
     * @param int $maxPages
     * @param bool $isset
     */
    public function __construct(
        private RouteParserInterface $routeParser,
        private int $currentPage = 1,
        private int $maxPages = 1,
        private bool $isset = false,
    ) {
    }

    /**
     * @return bool
     */
    public function isPaginated(): bool
    {
        return $this->isset;
    }

    /**
     * @param bool $isset
     */
    public function setPaginated(bool $isset): void
    {
        $this->isset = $isset;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getMaxPages(): int
    {
        return $this->maxPages;
    }

    /**
     * @param int $maxPages
     */
    public function setMaxPages(int $maxPages): void
    {
        $this->maxPages = $maxPages;
    }

    /**
     * @param RouteInterface|null $route
     */
    public function setRoute(RouteInterface $route = null): void
    {
        $this->route = $route;
    }

    /**
     * @return RouteInterface
     */
    public function getRoute(): RouteInterface
    {
        return $this->route;
    }

    /**
     * @return RouteParserInterface
     */
    public function getRouteParser(): RouteParserInterface
    {
        return $this->routeParser;
    }

    /**
     * @param array $queryParams
     */
    public function setQueryParams(array $queryParams = []): void
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }
}
