<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Pivot;

/**
 * Class GrRo
 * @package Netzwolke\Manager\Model\Pivot
 */
class GrRo extends BasePivot
{

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $table = 'grRo_pivot';

    /**
     * @var string
     */
    protected $connection = 'default';

    /**
     * @var bool
     */
    public $incrementing = true;
}
