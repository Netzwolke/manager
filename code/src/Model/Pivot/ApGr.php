<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Pivot;

/**
 * Class Routes
 * @package netzwolke\Model
 */
class ApGr extends BasePivot
{

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $table = 'apGr_pivot';

    /**
     * @var string
     */
    protected $connection = 'default';

    /**
     * @var bool
     */
    public $incrementing = true;
}
