<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model\Pivot;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class BasePivot
 * @package Netzwolke\Manager\Model\Pivot
 */
class BasePivot extends Pivot
{
    /**
     * @return bool
     */
    public static function hasTable(): bool
    {
        return Manager::schema()->hasTable((new static())->getTable());
    }
}
