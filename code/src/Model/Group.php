<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Netzwolke\Manager\Model\Pivot\ApGr;
use Netzwolke\Manager\Model\Pivot\GrRo;
use Netzwolke\Manager\Model\Pivot\GrUs;

/**
 * Class Group
 * @package Netzwolke\Manager\Model
 */
class Group extends BaseModel
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]|bool
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'default';

    /**
     * @return BelongsToMany
     */
    public function appliances(): BelongsToMany
    {
        return $this->belongsToMany(Appliance::class, 'apGr_pivot', 'groupId', 'applianceId')
            ->using(ApGr::class)
            ->withTimestamps();
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'grRo_pivot', 'groupId', 'roleId')
            ->using(GrRo::class)
            ->withTimestamps();
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'grUs_pivot', 'roleId', 'userId')
            ->using(GrUs::class)
            ->withTimestamps();
    }
}
