<?php /** @noinspection ALL */


namespace Netzwolke\Manager\Model;

use Netzwolke\Manager\Model\Pivot\ApGr;

/**
 * Class Appliance
 * @package Netzwolke\Manager\Model
 */
class Appliance extends BaseModel
{

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $table = 'appliances';

    /**
     * @var string
     */
    protected $connection = 'default';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'apGr_pivot', 'applianceId', 'groupId')
            ->using(ApGr::class)
            ->withTimestamps();
    }


}//end class
