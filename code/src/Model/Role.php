<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Model;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Netzwolke\Manager\Model\Pivot\GrRo;
use Netzwolke\Manager\Model\Pivot\UsRo;

/**
 * Class Role
 * @package Netzwolke\Manager\Model
 */
class Role extends BaseModel
{

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var string
     */
    protected $connection = 'default';

    /**
     * @return BelongsToMany
     */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'grRo_pivot', 'roleId', 'groupId')
            ->using(GrRo::class)
            ->withTimestamps();
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'usRo_pivot', 'roleId', 'userId')
            ->using(UsRo::class)
            ->withTimestamps();
    }
}
