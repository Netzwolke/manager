<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Psr\Http\Message\ResponseInterface;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class GroupController
 * @package Netzwolke\Manager\Controller
 */
class GroupController
{
    /**
     * @param ResponseInterface $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(ResponseInterface $response, Twig $twig): ResponseInterface
    {
        return $twig->render($response, 'res/group/index.twig');
    }
}
