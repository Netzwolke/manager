<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Model\Role;
use Netzwolke\Manager\Model\User;
use Psr\Http\Message\ResponseInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RouteContext;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class UserController
 * @package netzwolke\Controller
 */
class UserController
{

    /**
     * @param $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index($response, Twig $twig): ResponseInterface
    {
        $users = User::all();
        return $twig->render($response, 'res/user/index.twig', compact('users'));
    }

    /**
     * @param $response
     * @param $id
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function show($response, $id, Twig $twig): ResponseInterface
    {
        $user = User::find($id);
        return $twig->render($response, 'res/user/show.twig', compact('user'));
    }

    /**
     * @param $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function create($response, Twig $twig): ResponseInterface
    {
        $roles = Role::orderBy('power', 'asc')->get();
        return $twig->render($response, 'res/user/create.twig', compact('roles'));
    }

    /**
     * @param $request
     * @param $response
     * @param RouteParserInterface $routeParser
     * @return ResponseInterface
     */
    public function store($request, $response, RouteParserInterface $routeParser): ResponseInterface
    {
        $create = $request->getParsedBody();
        User::create($create);

        $url = $routeParser->urlFor('users.index');
        return $response->withHeader('Location', $url);
    }

    /**
     * @param $response
     * @param $id
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function edit($response, $id, Twig $twig): ResponseInterface
    {
        $user = User::find($id);
        return $twig->render($response, 'res/user/edit.twig', compact('user'));
    }

    /**
     * @param $request
     * @param $response
     * @param $id
     * @return ResponseInterface
     */
    public function update($request, $response, $id): ResponseInterface
    {
        $user = User::find($id);
        $update = $request->getParsedBody();
        $user->fill($update);
        $user->save();

        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->urlFor('users.index');
        return $response->withHeader('Location', $url);
    }

    /**
     * @param $request
     * @param $response
     * @param $id
     * @return ResponseInterface
     */
    public function delete($request, $response, $id): ResponseInterface
    {
        User::destroy($id);
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->urlFor('users.index');
        return $response->withHeader('Location', $url);
    }
}
