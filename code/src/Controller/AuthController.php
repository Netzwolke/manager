<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Factory\DB\TableManager;
use Netzwolke\Manager\Resources\Output\MessengerInterface;
use Netzwolke\Manager\Resources\WebStorage\Auth;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class AuthController
 * @package netzwolke\Controller
 */
class AuthController
{

    public function login(ServerRequestInterface $request, ResponseInterface $response, Auth $auth): ResponseInterface
    {

        $code = $auth->login($request);
        return $response->withStatus($code)->withHeader('location', $auth->getLastUrl());
    }

    /**
     * @param ResponseInterface $response
     * @param Auth $auth
     * @param MessengerInterface $messenger
     * @return ResponseInterface
     */
    public function logout(ResponseInterface $response, Auth $auth, MessengerInterface $messenger): ResponseInterface
    {
        $messenger->addSuccess("Successful logout!");
        $url = $auth->getLastUrl();
        $code = $auth->logout();
        return $response->withStatus($code)->withHeader('location', $url);
    }

    /**
     * @param ResponseInterface $response
     * @param Auth $auth
     * @param RouteParserInterface $parser
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function authenticate(
        ResponseInterface $response,
        Auth $auth,
        RouteParserInterface $parser,
        Twig $twig
    ): ResponseInterface {
        if ($auth->isAuth()) {
            return $response->withHeader('location', $parser->urlFor('auth.dashboard'));
        }
        return $twig->render($response, 'static/login/authenticate.twig');
    }

    public function dashboard(
        ResponseInterface $response,
        Twig $twig,
    ): ResponseInterface {
        return $twig->render($response, 'static/login/dashboard.twig');
    }
    public function remoteRebuild(
        ResponseInterface $response,
        Auth $auth,
        RouteParserInterface $parser,
        MessengerInterface $messenger,
    ): ResponseInterface {
        $client = $auth->getRemoteClient();
        $promise = $client->request(
            'POST',
            $parser->urlFor('auth.rebuild'),
            ['form_params' => ['key' => $auth->getKey()]]
        );
        $messenger->addSuccess((string)$promise->getBody());
        return $response->withStatus(302)->withHeader('location', $parser->urlFor('home'));
    }


    public function localRebuild(
        ResponseInterface $response,
        Auth $auth,
        RouteParserInterface $parser,
        MessengerInterface $messenger,
    ): ResponseInterface {
        $client = $auth->getLocalClient();
        $promise = $client->request(
            'POST',
            $parser->urlFor('auth.rebuild'),
            [
                'form_params' => ['key' => $auth->getKey()]
            ]
        );
        $messenger->addSuccess((string)$promise->getBody());
        return $response->withStatus(302)->withHeader('location', $parser->urlFor('home'));
    }

    public function rebuild(
        ResponseInterface $response,
        TableManager $manager,
    ): ResponseInterface {
        $manager->down($manager->loadMigrations());
        $manager->up($manager->loadMigrations());
        $manager->seed($manager->loadSeeds());
        $promise = json_encode(['status' => 'OK', 'database' => 'rebuild']);
        $response->getBody()->write($promise);
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json');
    }
}
