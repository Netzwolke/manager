<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Factory\DB\Manager;
use Netzwolke\Manager\Factory\DB\TableManager;
use Netzwolke\Manager\Resources\Output\MessengerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Views\Twig;

/**
 * Class DashController
 * @package Netzwolke\Manager\Controller
 */
class DashController
{
    /**
     * @param ResponseInterface $response
     * @param Twig $twig
     * @param TableManager $manager
     * @return ResponseInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function databaseDash(ResponseInterface $response, Twig $twig, TableManager $manager): ResponseInterface
    {
        [$migrations, $seeds] = $manager->loadAllClasses();
        return $twig->render($response, 'static/database/dash.twig', compact('migrations', 'seeds', 'manager'));
    }

    public function databaseExecute(
        ResponseInterface $response,
        ServerRequestInterface $request,
        RouteParserInterface $parser,
        Manager $manager,
        MessengerInterface $messenger,
        TableManager $tableManager
    ): ResponseInterface {
        $requestArray = $request->getParsedBody();
        $drops = $requestArray['drops'] ?? [];
        $migrations = $requestArray['migrations'] ?? [];
        $seeds = $requestArray['seeds'] ?? [];

        $dropClasses = $manager->getClasses($tableManager->loadMigrations(), $drops);
        if (!empty($drops)) {
            $tableManager->down($dropClasses);
            $messenger->addSuccess('Drops: ' . json_encode($drops));
        }

        $migrationClasses = $manager->getClasses($tableManager->loadMigrations(), $migrations);
        if (!empty($migrations)) {
            $tableManager->up($migrationClasses);
            $messenger->addSuccess('Migrations: ' . json_encode($migrations));
        }

        $seedClasses = $manager->getClasses($tableManager->loadSeeds(), $seeds);
        if (!empty($seeds)) {
            $tableManager->seed($seedClasses);
            $messenger->addSuccess('Seeds: ' . json_encode($seeds));
        }

        return $response->withHeader('location', $parser->urlFor('dash.database'));
    }
}
