<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Model\Appliance;
use Netzwolke\Manager\Model\Group;
use Psr\Http\Message\ResponseInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RouteContext;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ApplianceController
 * @package Netzwolke\Manager\Controller
 */
class ApplianceController
{
    /**
     * @param $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index($response, Twig $twig): ResponseInterface
    {
        $appliances = Appliance::all();
        return $twig->render($response, 'res/appliance/index.twig', compact('appliances'));
    }

    /**
     * @param $response
     * @param $id
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function show($response, $id, Twig $twig): ResponseInterface
    {
        $appliance = Appliance::find($id);
        $groups = Group::whereDoesntHave('appliances', function ($query) use ($id) {
            $query->where('applianceId', $id);
        })->get();
        return $twig->render($response, 'res/appliance/show.twig', compact('appliance', 'groups'));
    }

    /**
     * @param $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function create($response, Twig $twig): ResponseInterface
    {
        return $twig->render($response, 'res/appliance/create.twig');
    }

    /**
     * @param $request
     * @param $response
     * @param RouteParserInterface $routeParser
     * @return ResponseInterface
     */
    public function store($request, $response, RouteParserInterface $routeParser): ResponseInterface
    {
        $create = $request->getParsedBody();
        Appliance::create($create);

        $url = $routeParser->urlFor('appliances.index');
        return $response->withHeader('Location', $url);
    }

    /**
     * @param $response
     * @param $id
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function edit($response, $id, Twig $twig): ResponseInterface
    {
        $appliance = Appliance::find($id);
        return $twig->render($response, 'res/appliance/edit.twig', compact('appliance'));
    }

    /**
     * @param $request
     * @param $response
     * @param $id
     * @return ResponseInterface
     */
    public function update($request, $response, $id): ResponseInterface
    {
        $user = Appliance::find($id);
        $update = $request->getParsedBody();
        $user->fill($update);
        $user->save();

        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->urlFor('appliances.index');
        return $response->withHeader('Location', $url);
    }

    /**
     * @param $request
     * @param $response
     * @param $id
     * @return ResponseInterface
     */
    public function delete($request, $response, $id): ResponseInterface
    {
        Appliance::destroy($id);
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->urlFor('appliances.index');
        return $response->withHeader('Location', $url);
    }
}
