<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller\v1;

use Netzwolke\Manager\Model\Group;
use Psr\Http\Message\ResponseInterface;

/**
 * Class GroupController
 * @package Netzwolke\Manager\Controller
 */
class GroupController
{
    /**
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function index(ResponseInterface $response): ResponseInterface
    {
        $groups = Group::all();
        $groups = $groups ? $groups->toJson() : json_encode(['Groups' => 'empty']);
        $response->getBody()->write($groups);
        return $response->withHeader('content-type', 'application/json');
    }
}
