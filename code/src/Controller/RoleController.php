<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Model\Role;
use Psr\Http\Message\ResponseInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RouteContext;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class RoleController
 * @package netzwolke\Controller
 */
class RoleController
{

    /**
     * @param $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index($response, Twig $twig): ResponseInterface
    {

        $roles = Role::all()->paginate();
        return $twig->render($response, 'res/role/index.twig', compact('roles'));
    }

    /**
     * @param $response
     * @param $id
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function show($response, $id, Twig $twig): ResponseInterface
    {
        $role = Role::find($id);
        return $twig->render($response, 'res/role/show.twig', compact('role'));
    }

    /**
     * @param $response
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function create($response, Twig $twig): ResponseInterface
    {
        return $twig->render($response, 'res/role/create.twig');
    }

    /**
     * @param $request
     * @param $response
     * @param RouteParserInterface $routeParser
     * @return ResponseInterface
     */
    public function store($request, $response, RouteParserInterface $routeParser): ResponseInterface
    {
        $create = $request->getParsedBody();
        Role::create($create);

        $url = $routeParser->urlFor('roles.index');
        return $response->withHeader('Location', $url);
    }

    /**
     * @param $response
     * @param $id
     * @param Twig $twig
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function edit($response, $id, Twig $twig): ResponseInterface
    {
        $role = Role::find($id);
        return $twig->render($response, 'res/role/edit.twig', compact('role'));
    }

    /**
     * @param $request
     * @param $response
     * @param $id
     * @return ResponseInterface
     */
    public function update($request, $response, $id): ResponseInterface
    {
        $role = Role::find($id);
        $update = $request->getParsedBody();
        $role->fill($update);
        $role->save();

        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->urlFor('roles.index');
        return $response->withHeader('Location', $url);
    }

    /**
     * @param $request
     * @param $response
     * @param $id
     * @return ResponseInterface
     */
    public function delete($request, $response, $id): ResponseInterface
    {
        Role::destroy($id);
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $url = $routeParser->urlFor('roles.index');
        return $response->withHeader('Location', $url);
    }
}
