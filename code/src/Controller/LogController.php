<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Model\ErrorLog;
use Netzwolke\Manager\Model\UserLog;
use Psr\Http\Message\ResponseInterface;

/**
 * Class LogController
 * @package Netzwolke\Manager\Controller
 */
class LogController
{
    public function userLog(ResponseInterface $response): ResponseInterface
    {
        $response->getBody()->write(UserLog::orderBy('id', 'desc')->get()->toJson());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function errorLog(ResponseInterface $response): ResponseInterface
    {
        $json = ErrorLog::orderBy('id', 'desc')->get()->toJson();
        $response->getBody()->write($json);
        return $response->withHeader('Content-Type', 'application/json');
    }
}
