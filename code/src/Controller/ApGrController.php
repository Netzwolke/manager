<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Controller;

use Netzwolke\Manager\Model\Appliance;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteParserInterface;

/**
 * Class ApGrController
 * @package Netzwolke\Manager\Controller
 */
class ApGrController
{

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param $id
     * @param RouteParserInterface $parser
     * @return ResponseInterface
     */
    public function addGroup(
        ServerRequestInterface $request,
        ResponseInterface $response,
        $id,
        RouteParserInterface $parser
    ): ResponseInterface {
        $array = $request->getParsedBody();
        $appliance = Appliance::find($id);
        $appliance->groups()->attach($array['groupId']);

        return $response->withHeader('location', $parser->urlFor('appliances.show', ['id' => $id]));
    }

    /**
     * @param ResponseInterface $response
     * @param $id
     * @param $groupId
     * @param RouteParserInterface $parser
     * @return ResponseInterface
     */
    public function removeGroup(
        ResponseInterface $response,
        $id,
        $groupId,
        RouteParserInterface $parser
    ): ResponseInterface {
        $appliance = Appliance::find($id);
        $appliance->groups()->detach($groupId);
        return $response->withHeader('location', $parser->urlFor('appliances.show', ['id' => $id]));
    }
}
