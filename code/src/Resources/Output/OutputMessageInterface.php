<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\Output;

/**
 * Interface OutputMessageInterface
 * @package Netzwolke\Manager\Resources\Output
 */
interface OutputMessageInterface
{
    /**
     *
     */
    public const KEY     = '__Message__';
    /**
     *
     */
    public const ERROR   = 'Error';
    /**
     *
     */
    public const WARNING = 'Warning';
    /**
     *
     */
    public const SUCCESS = 'Success';
}
//end interface
