<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\Output;

/**
 * Interface MessageInterface
 * @package Netzwolke\Manager\Resources\Output
 */
interface MessageInterface
{
    public function add(string $type, string $message): void;

    public function get(string $type): array;

    public function clear(string $type): void;
}
