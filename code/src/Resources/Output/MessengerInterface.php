<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\Output;

/**
 * Interface MessengerInterface
 * @package Netzwolke\Manager\Resources\Output
 */
interface MessengerInterface
{
    public function addSuccess(string $message): void;

    public function addWarning(string $message): void;

    public function addError(string $message): void;
}
