<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\Output;

use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\SessionWrapperInterface;

/**
 * Class Messenger
 * @package Netzwolke\Manager\Resources\Output
 */
class Messenger implements OutputMessageInterface, MessengerInterface
{
    /**
     * @var SessionWrapperInterface
     */
    private SessionWrapperInterface $session;
    /**
     * @var Message
     */
    private Message $message;

    public function __construct(SessionWrapperInterface $session, Message $message)
    {
        $this->session = $session;
        $this->message = $message;
    }

    public function check()
    {
        if (!$this->session->has(self::KEY)) {
            $this->session->set(self::KEY, $this->message);
        }
    }


    /**
     * @param string $message
     */
    public function addError(string $message): void
    {
        $this->add(self::ERROR, $message);
    }

    /**
     * @param string $message
     */
    public function addWarning(string $message): void
    {
        $this->add(self::WARNING, $message);
    }

    /**
     * @param string $message
     */
    public function addSuccess(string $message): void
    {
        $this->add(self::SUCCESS, $message);
    }

    /**
     * @param $type
     * @param string $message
     */
    public function add($type, string $message): void
    {
        $this->check();

        $MessageClass = $this->session->get(self::KEY);
        $MessageClass->add($type, $message);
    }

    /**
     * @param $type
     * @return mixed
     */
    public function get($type): array
    {
        $this->check();

        //Get Message in Session
        $message =  $this->session->get(self::KEY);

        //Get Type of Message
        return $message->get($type);
    }

    /**
     * @param $type
     * @return bool
     */
    public function has($type): bool
    {
        $this->check();
        //Get Message in Session
        $message =  $this->session->get(self::KEY);

        //Get Type of Message
        $output = $message->get($type);
        if ($output) {
            return true;
        }
        return false;
    }

    /**
     * @param $type
     */
    public function clear($type)
    {
        $this->check();

        //get Message in Session
        $message = $this->session->get(self::KEY);

        //Clear Message Array for $type
        $message->clear($type);
    }
}
