<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\Output;

/**
 * Class Message
 * @package Netzwolke\Manager\Resources\Output
 */
class Message implements OutputMessageInterface, MessageInterface
{

    private array $messages;

    /**
     * Message constructor.
     * @param array $errors
     * @param array $warnings
     * @param array $successes
     */
    public function __construct(
        $errors = [],
        $warnings = [],
        $successes = []
    ) {
        $this->messages = [
            self::ERROR => $errors,
            self::WARNING => $warnings,
            self::SUCCESS => $successes,
        ];
    }

    public function add(string $type, string $message): void
    {
        $this->messages[$type][] = $message;
    }


    public function get(string $type): array
    {
        return $this->messages[$type];
    }


    public function clear(string $type): void
    {
        unset($this->messages[$type]);
        $this->messages[$type] = [];
    }
}
