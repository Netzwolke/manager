<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\WebStorage\WrapperInterface;

/**
 * Interface BaseWrapperInterface
 * @package Netzwolke\Manager\Resources\WebStorage\WrapperInterface
 */
interface BaseWrapperInterface
{
    public function isAuth(): bool;

    public function getUserName(): string;
}
