<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\WebStorage\WrapperInterface;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface AuthWrapperInterface
 * @package Netzwolke\Manager\Resources\WebStorage\WrapperInterface
 */
interface AuthWrapperInterface
{
    public function isApp(ServerRequestInterface $request): int;

    public function login(ServerRequestInterface $request): int;

    public function logout(): int;
}
