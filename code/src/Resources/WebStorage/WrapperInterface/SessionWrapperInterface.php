<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\WebStorage\WrapperInterface;

use Netzwolke\Manager\Model\ModelInterface;

/**
 * Interface SessionWrapperInterface
 * @package Netzwolke\Manager\Resources\WebStorage\WrapperInterface
 */
interface SessionWrapperInterface extends BaseWrapperInterface
{
    /**
     * @param string $name
     * @param $value
     */
    public function add(string $name, $value): void;

    /**
     * @param string $name
     * @param $value
     */
    public function set(string $name, $value): void;

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name): mixed;

    public function remove(string $name): void;

    public function has(string $name): bool;

    public function getHistory(): array;

    public function setHistory(string $url): void;

    public function isAuth(): bool;

    public function setAuth(bool $isAuth): void;

    public function startSession(): void;

    public function destroySession(): void;

    public function getUserLog(): ModelInterface | null;

    public function setUserLog(ModelInterface $userLog): void;
}
