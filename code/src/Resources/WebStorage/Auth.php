<?php

/** @noinspection PhpUndefinedFieldInspection */

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\WebStorage;

use GuzzleHttp\Client;
use Netzwolke\Logger\NetzwolkeLoggerInterface;
use Netzwolke\Manager\Model\ModelInterface;
use Netzwolke\Manager\Model\User;
use Netzwolke\Manager\Model\UserLog;
use Netzwolke\Manager\Resources\Output\MessengerInterface;
use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\AuthWrapperInterface;
use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\SessionWrapperInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class Auth
 * @package Netzwolke\Manager\Resources\WebStorage
 */
class Auth implements AuthWrapperInterface
{
    private int $timeout = 3000; //in seconds

    public function __construct(
        private SessionWrapperInterface $session,
        private MessengerInterface $messenger,
        private Client $remoteClient,
        private Client $localClient,
        private string $env,
        private string $key,
        private string $version,
        private NetzwolkeLoggerInterface $logger
    ) {
    }

    public function isApp(ServerRequestInterface $request): int
    {
        if ($request->getMethod() === "POST") {
            $param = $request->getParsedBody();
            if (isset($param['key'])) {
                if (password_verify($this->key, $param['key'])) {
                    return 200;
                }
                return 403;
            }
        }
        return 302;
    }

    /**
     * @return Client
     */
    public function getRemoteClient(): Client
    {
        return $this->remoteClient;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getEnv(): string
    {
        return $this->env;
    }

    /**
     * @return Client
     */
    public function getLocalClient(): Client
    {
        return $this->localClient;
    }

    public function getKey(): string
    {
        $options = [
            'cost' => 12,
        ];
        return password_hash($this->key, PASSWORD_BCRYPT, $options);
    }

    public function isAuth(): bool
    {
        if ($this->session->isAuth()) {
            if ($this->isExpired()) {
                $this->messenger->addError('Timeout!');
                $this->logout();
                return false;
            }
            return true;
        }
        return false;
    }

    public function refreshUserLog()
    {
        $log = $this->session->getUserLog();
        $dbLog = UserLog::find($log->id);
        if ($dbLog) {
            $dbLog->signedIn = time();
            $dbLog->save();
        }
        $this->session->setUserLog($dbLog);
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }


    private function isExpired(): bool
    {
        $log = $this->session->getUserLog();
        $dbLog = UserLog::find($log->id);
        if ($dbLog) {
            if ($dbLog->signedIn + $this->timeout >= time()) {
                return false;
            }
        }

        return true;
    }

    public function getUserName(): string
    {
        return $this->getUser()->name ?? 'No Username';
    }

    /**
     * @return mixed
     */
    public function getUser(): ModelInterface
    {
        $userLog = $this->session->getUserLog();
        return User::find($userLog->userId);
    }

    private function makeUserLog(int $userId, string $ip, string $userAgent): UserLog
    {


        $userLog_ref = [
            'userId' => $userId,
            'ip' => $ip,
            'userAgent' => $userAgent,
            'logId' => uniqid('LID', true),
            'signedIn' => time(),
            'attempts' => 1
        ];
        return UserLog::create($userLog_ref);
    }

    public function login(ServerRequestInterface $request): int
    {
        $post = $request->getParsedBody();
        $name = $post['name'];
        $password = $post['password'];
        $param = $request->getServerParams();

        $user = User::where('name', $name)->first();
        if ($user === null) {
            $this->messenger->addError('No User Found for ' . $name);
            return 401;
        }

        if ($user->password === $password) {
            $this->session->setAuth(true);
            $userLog = $this->makeUserLog($user->id, $param['HTTP_HOST'], $param['HTTP_USER_AGENT']);
            $this->session->setUserLog($userLog);
            $this->messenger->addSuccess('Welcome ' . $user->name);
            return 200;
        }
        $params = $request->getServerParams();
        $remoteAddress = $params['REMOTE_ADDR'];
        $this->logger->fire(
            'critical',
            type: 'Wrong Login',
            message: "User $name from $remoteAddress",
            file: get_class($this)
        );
        $this->messenger->addError("Wrong Password");
        return 401;
    }

    /**
     * @return int
     */
    public function logout(): int
    {

        $messages = $this->session->getMessages();
        $this->session->destroySession();
        $this->session->startSession();
        $this->session->setMessages($messages);

        return 200;
    }

    public function getLastUrl(): string
    {
        $array = $this->session->getHistory() ?? [];
        $url = end($array);
        return $url ? $url : '/';
    }

    public function getHistory(): array
    {
        return $this->session->getHistory();
    }
}
