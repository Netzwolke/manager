<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Resources\WebStorage;

use Netzwolke\Manager\Model\ModelInterface;
use Netzwolke\Manager\Model\UserLog;
use Netzwolke\Manager\Resources\Output\MessageInterface;
use Netzwolke\Manager\Resources\Output\OutputMessageInterface;
use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\SessionWrapperInterface;

/**
 * Class Session
 * @package Netzwolke\Manager\Resources\WebStorage
 */
class Session implements SessionWrapperInterface
{

    public function setAuth(bool $isAuth): void
    {
        $this->set('authenticated', $isAuth);
    }

    public function setHistory(string $url): void
    {
        $this->add('history', $url);
    }

    public function getHistory(): array
    {
        return $this->get('history') ?? [];
    }

    public function isAuth(): bool
    {
        if ($this->has('authenticated')) {
            return $this->get('authenticated');
        }
        $this->set('authenticated', false);
        return false;
    }

    public function setMessages(MessageInterface $messages): void
    {
        $this->set(OutputMessageInterface::KEY, $messages);
    }

    public function getMessages(): MessageInterface
    {
        return $this->get(OutputMessageInterface::KEY);
    }

    /**
     * @param UserLog $userLog
     */
    public function setUserLog(ModelInterface $userLog): void
    {
        $this->set('_userLog_', $userLog);
    }

    /**
     * @return mixed|null
     */
    public function getUserLog(): Userlog | null
    {
        if ($this->has('_userLog_')) {
            return $this->get('_userLog_');
        }
        return null;
    }

    public function destroySession(): void
    {
        session_destroy();
    }
    public function startSession(): void
    {
        session_start();
    }


    /**
     * @param string $name
     * @param $value
     */
    public function set(string $name, $value): void
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            $_SESSION[$name] = $value;
        }
    }

    /**
     * @param string $name
     * @param $value
     */
    public function add(string $name, $value): void
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            $_SESSION[$name][] = $value;
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name): mixed
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            return $_SESSION[$name];
        }
        return null;
    }

    public function remove(string $name): void
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            unset($_SESSION[$name]);
        }
    }

    public function has(string $name): bool
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            if (array_key_exists($name, $_SESSION)) {
                return true;
            }
        }
        return false;
    }

    public function getUserName(): string
    {
        return "no";// TODO: Implement getUserName() method.
    }
}
