<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Twig;

use Netzwolke\Manager\Factory\Twig\Functions\FormTwigFunction;
use Netzwolke\Manager\Factory\Twig\Functions\CoreTwigFunction;
use Netzwolke\Manager\Factory\Twig\Functions\PaginateTwigFunction;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TwigExtension
 * @package netzwolke\Factory\Twig
 */
class TwigExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('isAuth', [CoreTwigFunction::class, 'isAuth']),
            new TwigFunction('getUserName', [CoreTwigFunction::class, 'getUserName']),
            new TwigFunction('getMessage', [CoreTwigFunction::class, 'getMessage']),
            new TwigFunction('hasMessage', [CoreTwigFunction::class, 'hasMessage']),
            new TwigFunction('method', [FormTwigFunction::class, 'method'], ['is_safe' => ['html']]),
            new TwigFunction('makeId', [FormTwigFunction::class, 'makeId'], ['is_safe' => ['html', 'all']]),
            new TwigFunction('getHost', [CoreTwigFunction::class, 'getHost']),
            new TwigFunction('getVersion', [CoreTwigFunction::class, 'getVersion']),
            new TwigFunction('getEnv', [CoreTwigFunction::class, 'getEnv']),
            new TwigFunction('getTimeout', [CoreTwigFunction::class, 'getTimeout']),
            new TwigFunction('getPageUrl', [PaginateTwigFunction::class, 'getPageUrl']),
            new TwigFunction('getCurrentPage', [PaginateTwigFunction::class, 'getCurrentPage']),
            new TwigFunction('getMaxPages', [PaginateTwigFunction::class, 'getMaxPages']),
            new TwigFunction('isPaginated', [PaginateTwigFunction::class, 'isPaginated']),



        ];
    }
}
