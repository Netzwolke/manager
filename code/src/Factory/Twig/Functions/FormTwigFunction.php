<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Twig\Functions;

/**
 * Class FormFunctions
 * @package netzwolke\Factory\Twig
 */
class FormTwigFunction
{

    /**
     * @param $method
     * @return string
     */
    public function method($method): string
    {
        return "<input type='hidden' name='_METHOD' value='$method'>";
    }

    public function makeId(): string
    {
        return 'modal' . uniqid('id' . date_create()->format('v'));
    }
}
