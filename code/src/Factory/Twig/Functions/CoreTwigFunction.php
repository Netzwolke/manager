<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Twig\Functions;

use Netzwolke\Manager\Resources\Output\Messenger;
use Netzwolke\Manager\Resources\WebStorage\Auth;
use Slim\Interfaces\RouteCollectorInterface;

/**
 * Class TwigFunctions
 * @package netzwolke\Factory\Twig
 */
class CoreTwigFunction
{
    public function __construct(
        private Messenger $messenger,
        private Auth $auth,
        private RouteCollectorInterface $collector
    ) {
    }

    public function isAuth(): bool
    {
        return $this->auth->isAuth();
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->auth->getUserName();
    }

    /**
     * @param $type
     * @return bool
     */
    public function hasMessage($type): bool
    {
        return $this->messenger->has($type);
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getMessage($type): mixed
    {
        $message =  $this->messenger->get($type);
        $this->messenger->clear($type);
        return $message;
    }

    public function getHost(): string
    {
        return $this->collector->getNamedRoute('home')->getName();
    }

    public function getVersion(): string
    {
        return $this->auth->getVersion();
    }

    public function getEnv(): string
    {
        return $this->auth->getEnv();
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->auth->getTimeout();
    }
}
