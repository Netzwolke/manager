<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Twig\Functions;

use Netzwolke\Manager\Model\Resources\PaginationInterface;

/**
 * Class PaginateFunctions
 * @package Netzwolke\Manager\Factory\Twig\Functions
 */
class PaginateTwigFunction
{
    /**
     * PaginateFunctions constructor.
     * @param PaginationInterface $pagination
     */
    public function __construct(
        private PaginationInterface $pagination,
    ) {
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->pagination->getCurrentPage();
    }

    /**
     * @return int
     */
    public function getMaxPages(): int
    {
        return $this->pagination->getMaxPages();
    }

    /**
     * @return bool
     */
    public function isPaginated(): bool
    {
        return $this->pagination->isPaginated();
    }

    /**
     * @param int $page
     * @return string
     */
    public function getPageUrl(int $page): string
    {
        $parser = $this->pagination->getRouteParser();
        $routeName = $this->pagination->getRoute()->getName();
        $args = $this->pagination->getRoute()->getArguments();
        $queryParams = $this->pagination->getQueryParams();
        $queryParams['page'] = $page;

        return $parser->urlFor(
            $routeName,
            $args,
            $queryParams
        );
    }
}
