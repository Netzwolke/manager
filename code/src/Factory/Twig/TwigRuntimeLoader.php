<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Twig;

use Netzwolke\Manager\Factory\Twig\Functions\FormTwigFunction;
use Netzwolke\Manager\Factory\Twig\Functions\CoreTwigFunction;
use Netzwolke\Manager\Factory\Twig\Functions\PaginateTwigFunction;
use Psr\Container\ContainerInterface;
use Twig\RuntimeLoader\RuntimeLoaderInterface;

/**
 * Class TwigRuntimeLoader
 * @package netzwolke\Factory\Twig
 */
class TwigRuntimeLoader implements RuntimeLoaderInterface
{

    /**
     * TwigRuntimeLoader constructor.
     * @param ContainerInterface $container
     */
    public function __construct(
        private ContainerInterface $container
    ) {
    }

    /**
     * @param string $class
     * @return mixed
     */
    public function load(string $class): mixed
    {
        if (FormTwigFunction::class === $class) {
            return $this->container->get($class);
        }
        if (CoreTwigFunction::class === $class) {
            return $this->container->get($class);
        }
        if (PaginateTwigFunction::class === $class) {
            return $this->container->get($class);
        }
        return null;
    }
}
