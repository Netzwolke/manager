<?php

/** @noinspection PhpPossiblePolymorphicInvocationInspection */

declare(strict_types=1);

namespace Netzwolke\Manager\Factory;

use Netzwolke\Manager\Controller\ApGrController;
use Netzwolke\Manager\Controller\ApplianceController;
use Netzwolke\Manager\Controller\AuthController;
use Netzwolke\Manager\Controller\DashController;
use Netzwolke\Manager\Controller\GroupController;
use Netzwolke\Manager\Controller\LogController;
use Netzwolke\Manager\Controller\RoleController;
use Netzwolke\Manager\Controller\UserController;
use Netzwolke\Manager\Factory\Middleware\AuthMiddleware;
use Netzwolke\Manager\Resources\Output\MessengerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\App;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RouteCollectorProxy;

/**
 * Class RoutesFactory
 * @package netzwolke\Factory
 */
class RoutesFactory
{
    private array $routes;

    public function __construct(App $app)
    {
        $this->setRoutePatterns();
        $this->setRoutes($app);
        //add route parser to container AFTER routes have been added to App
        $this->setRouteCollector($app);
        $this->setCallableResolver($app);
        $this->setResponseFactory($app);
    }

    private function setResponseFactory(App $app): void
    {
        $container = $app->getContainer();
        $container->set(ResponseFactoryInterface::class, $app->getResponseFactory());
    }

    private function setCallableResolver(App $app): void
    {
        $container = $app->getContainer();
        $container->set(CallableResolverInterface::class, $app->getCallableResolver());
    }

    /**
     * @param App $app
     */
    public function setRouteCollector(App $app)
    {
        $container = $app->getContainer();
        $container->set(RouteCollectorInterface::class, $app->getRouteCollector());
    }


    /**
     * @param App $app
     */
    public function setRoutes(App $app)
    {

        //Auth routes
        $app->post('/login', [AuthController::class, 'login'])->setName('auth.login');
        $app->post('/logout', [AuthController::class, 'logout'])->setName('auth.logout');
        $app->get('/authentication', [AuthController::class, 'authenticate'])->setName('auth.authenticate');

        //Api routes
        $app->group('/api/v1', function (RouteCollectorProxy $app) {
            $this->resources(
                $app,
                '/groups',
                \Netzwolke\Manager\Controller\v1\GroupController::class,
                ['name' => 'v1.groups']
            );
        });

        //App routes
        $app->group('/admin', function (RouteCollectorProxy $app) {
            $this->resources($app, '/roles', RoleController::class);
            $this->resources($app, '/users', UserController::class);
            $app->post('/appliances/{id}/group/add', [ApGrController::class, 'addGroup'])
                ->setName('appliances.addGroup');
            $app->delete('/appliances/{id}/group/{groupId}/remove', [ApGrController::class, 'removeGroup'])
                ->setName('appliances.removeGroup');

            $this->resources($app, '/appliances', ApplianceController::class);
            $this->resources($app, '/groups', GroupController::class);

            $app->get('/userLogs', [LogController::class, 'userLog'])->setName('log.userLog');
            $app->get('/errorLogs', [LogController::class, 'errorLog'])->setName('log.errorLog');

            $app->get('/database/dash', [DashController::class, 'databaseDash'])->setName('dash.database');
            $app->post('/database/execute', [DashController::class, 'databaseExecute'])->setName('dash.database.execute');

            $app->post('/database/rebuild', [AuthController::class, 'rebuild'])->setName('auth.rebuild');
            $app->get('/dashboard', [AuthController::class, 'dashboard'])->setName('auth.dashboard');
            $app->get('/db/rebuild/remote', [AuthController::class, 'remoteRebuild'])->setName('auth.remoteRebuild');
            $app->get('/db/rebuild/local', [AuthController::class, 'localRebuild'])->setName('auth.localRebuild');
        })->add(AuthMiddleware::class . '::process');


        //redirects
        $app->get('/', function (ResponseInterface $response, RouteParserInterface $parser) {
            return $response->withHeader('location', $parser->urlFor('auth.dashboard'));
        })->setName('home');
        $app->map(
            ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
            '/{routes:.+}',
            function (ResponseInterface $response, RouteParserInterface $parser, MessengerInterface $messenger) {
                $messenger->addError('Error: 404 URL not Found!');
                return $response->withHeader('location', $parser->urlFor('home'));
            }
        )->setName('error.notFound');
    }

    private function setRoutePatterns(): void
    {
        $this->setRoute('index', 'get', '');
        $this->setRoute('create', 'get', '/create');
        $this->setRoute('edit', 'get', '/{id}/edit');
        $this->setRoute('show', 'get', '/{id}/show');
        $this->setRoute('store', 'post', '/store');
        $this->setRoute('update', 'put', '/{id}/update');
        $this->setRoute('delete', 'delete', '/{id}/delete');
    }

    /**
     * @param string $name
     * @param string $method
     * @param string $pattern
     */
    private function setRoute(string $name, string $method, string $pattern): void
    {
        $this->routes[$name]['method'] = $method;
        $this->routes[$name]['pattern'] = $pattern;
    }

    /**
     * @param RouteCollectorProxy $app
     * @param string $pattern
     * @param string $callable
     * @param array|null $options
     */
    private function resources(RouteCollectorProxy $app, string $pattern, string $callable, array $options = null)
    {
        $routes = [
            'index',
            'create',
            'edit',
            'show',
            'store',
            'update',
            'delete'
        ];

        $name = isset($options['name']) ? $options['name'] : null;
        $routes = isset($options['only']) ? $options['only'] : $routes;
        $middleware = isset($options['middleware']) ?  $options['middleware'] : null;

        if (is_null($name)) {
            $name = $pattern;
        }

        if (!(!str_contains($name, '/'))) {
            $name_ref = explode('/', $name);
            $name_ref = array_slice($name_ref, 1);
            $name = implode('.', $name_ref);
        }


        $routes = array_intersect_key($this->routes, array_flip($routes));
        foreach ($routes as $routeName => $options) {
            $method = $options['method'];
            $url = $pattern . $options['pattern'];
            $route = $app->$method($url, [$callable, $routeName]);
            $route->setName($name . '.' . $routeName);
            if ($middleware) {
                $route->add($middleware);
            }
        }
    }
}
