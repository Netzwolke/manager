<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory;

use Netzwolke\Manager\Factory\Middleware\DebugMiddleware;
use Netzwolke\Manager\Factory\Middleware\MigrationMiddleware;
use Netzwolke\Manager\Factory\Middleware\PaginationMiddleware;
use Netzwolke\Manager\Factory\Middleware\SessionMiddleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Slim\Middleware\MethodOverrideMiddleware;
use Slim\Views\TwigMiddleware;

/**
 * Class MiddlewareFactory
 * @package netzwolke\Factory
 */
class MiddlewareFactory
{
    public function __construct(App $app)
    {
        $this->addMiddleware($app);
    }

    /**
     * @param App $app
     */
    public function addMiddleware(App $app)
    {
        //Add PaginationMiddleware
        $app->add(PaginationMiddleware::class . "::process");

        $app->add(DebugMiddleware::class . '::process');

        //Add RoutingMiddleware before MethodOverrideMiddleware so the method is not overridden before routing


        //Add TwigMiddleware
        $app->add(TwigMiddleware::class . '::process');

        //Add SessionMiddleware
        $app->add(SessionMiddleware::class . "::process");

        $app->add(MigrationMiddleware::class . '::process');

        $app->addRoutingmiddleware();

        //Add MethodOverride middleware for method spoofing
        // e.g. POST request with _METHOD parameter PUT to
        // PUT request
        $app->add(MethodOverrideMiddleware::class . '::process');
        //Add ErrorMiddleware
        $app->add(ErrorMiddleware::class . '::process');
    }
}
