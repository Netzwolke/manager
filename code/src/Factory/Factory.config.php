<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory;

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__ . '/../../../config');
$dotenv->load();
return [
    'view' => [
        'path' => [
            '__main__' => '../src/View',
            '__content__' => 'content'
            ],
        'settings' => ['cache' => false]
        ],
    'env' =>  $_ENV['DEPLOY_ENV'] === '' ? 'dev' : $_ENV['DEPLOY_ENV'],
    'version' =>  $_ENV['APP_VERSION'] === '' ? 'dev' : $_ENV['APP_VERSION'],
    'key' => $_ENV['KEY'] ?? 'no Key',
    'model' => [
        'driver' => $_ENV['DRIVER'] ?? 'mysql',
        'host' => $_ENV['HOST'] ?? 'localhost',
        'port' => $_ENV['PORT'] ?? '3306',
        'database' => $_ENV['DATABASE'] ?? 'netzwolke_stg',
        'username' => $_ENV['USERNAME'] ?? 'root',
        'password' => $_ENV['PASSWORD'] ?? '123456',
        'charset' => $_ENV['CHARSET'] ?? 'utf8mb4',
        'collation' => $_ENV['COLLATION'] ?? 'utf8mb4_bin',
        'prefix' => $_ENV['PREFIX'] ?? '',
    ],
];
