<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory;

use Slim\App;

/**
 * Class AppFactory
 * @package netzwolke\Factory
 */
class AppFactory
{

    public function createApp(): App
    {

        //build app with DependencyInjectionFactory!
        $DI = $this->createDI();
        $app = $DI->getApp();

        //configure app!
        $this->createRoutes($app);
        $this->createModel($app);
        $this->createMiddleware($app);
        return $app;
    }

    public function createDI(): DependencyInjectionFactory
    {
        return new DependencyInjectionFactory();
    }

    /**
     * @param $app
     * @return RoutesFactory
     */
    public function createRoutes($app): RoutesFactory
    {
        return new RoutesFactory($app);
    }

    public function createModel(App $app): ModelFactory
    {
        return new ModelFactory($app);
    }

    public function createMiddleware(App $app): MiddlewareFactory
    {
        return new MiddlewareFactory($app);
    }
}
