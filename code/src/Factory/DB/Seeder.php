<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

use Netzwolke\Manager\Factory\DB\Seed\ApplianceSeed;
use Netzwolke\Manager\Factory\DB\Seed\GroupSeed;
use Netzwolke\Manager\Factory\DB\Seed\HttpRequestSeed;
use Netzwolke\Manager\Factory\DB\Seed\Pivot\ApGrSeed;
use Netzwolke\Manager\Factory\DB\Seed\RoleSeed;
use Netzwolke\Manager\Factory\DB\Seed\UserSeed;

/**
 * Class Seeder
 * @package netzwolke\Factory\DB\Seed\Migration
 */
class Seeder implements SeederInterface
{
    /**
     * Seeder constructor.
     * @param ExecutorInterface $executor
     */
    public function __construct(
        private ExecutorInterface $executor
    ) {
    }

    /**
     * @return string[]
     */
    public function loadSeeds(): array
    {
        return [
            RoleSeed::class,
            UserSeed::class,
            HttpRequestSeed::class,
            ApplianceSeed::class,
            GroupSeed::class,
            ApGrSeed::class,
        ];
    }
}
