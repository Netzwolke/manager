<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

/**
 * Interface MigratorInterface
 * @package netzwolke\Factory\DB
 */
interface MigratorInterface
{
    /**
     * @return array
     */
    public function loadMigrations(): array;
}
