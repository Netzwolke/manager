<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

/**
 * Interface ExecutorInterface
 * @package Netzwolke\Manager\Factory\DB
 */
interface ExecutorInterface
{
    public function execs(array $classes, string $function, array $params = []): void;
    public function exec(string $class, string $function, array $params = []): mixed;
}
