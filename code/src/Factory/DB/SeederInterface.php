<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

/**
 * Interface SeederInterface
 * @package netzwolke\Factory\DB
 */
interface SeederInterface
{
    public function loadSeeds(): array;
}
