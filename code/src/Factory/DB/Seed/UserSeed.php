<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

use Netzwolke\Manager\Factory\DB\SeedTrait;
use Netzwolke\Manager\Model\User;

/**
 * Class UserSeed
 * @package netzwolke\Factory\DB\Seed\Migration\Seed
 */
class UserSeed extends User implements SeedInterface
{
    use SeedTrait;

    public function default(): array
    {
        return [
            [
                'name' => 'Root',
                'password' => 'toor'
            ], [
                'name' => 'User',
                'password' => 'user'
            ], [
                'name' => 'Guest',
                'password' => 'guest'
            ]
        ];
    }
}
