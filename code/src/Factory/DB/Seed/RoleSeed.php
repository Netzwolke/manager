<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

use Netzwolke\Manager\Factory\DB\SeedTrait;
use Netzwolke\Manager\Model\Role;

/**
 * Class RoleSeed
 * @package netzwolke\Factory\DB\Seed\Migration\Seed
 */
class RoleSeed extends Role implements SeedInterface
{
    use SeedTrait;

    public function default(): array
    {
        return [
            [
                'name' => 'Admin',
                'power' => 10000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ], [
                'name' => 'User',
                'power' => 100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ], [
                'name' => 'Guest',
                'power' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ];
    }
}
