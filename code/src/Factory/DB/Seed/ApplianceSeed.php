<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

use Netzwolke\Manager\Factory\DB\SeedTrait;
use Netzwolke\Manager\Model\Appliance;

/**
 * Class ApplianceSeed
 * @package Netzwolke\Manager\Factory\DB\Seed
 */
class ApplianceSeed extends Appliance
{
    use SeedTrait;

    public function default(): array
    {
        return [
            [
                'name' => 'test',
                'url' => 'https://test.com',
                'disabled' => true
            ],
        ];
    }
}
