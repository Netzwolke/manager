<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

use Netzwolke\Manager\Factory\DB\SeedTrait;
use Netzwolke\Manager\Model\Group;

/**
 * Class GroupSeed
 * @package Netzwolke\Manager\Factory\DB\Seed
 */
class GroupSeed extends Group implements SeedInterface
{
    use SeedTrait;

    public function default(): array
    {
        return [
            [
                'name' => 'test',
            ]
        ];
    }
}
