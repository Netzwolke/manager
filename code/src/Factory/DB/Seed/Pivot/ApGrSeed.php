<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed\Pivot;

use Netzwolke\Manager\Factory\DB\Seed\SeedInterface;
use Netzwolke\Manager\Factory\DB\SeedTrait;
use Netzwolke\Manager\Model\Pivot\ApGr;

/**
 * Class ApUsSeed
 * @package Netzwolke\Manager\Factory\DB\Seed
 */
class ApGrSeed extends ApGr implements SeedInterface
{
    use SeedTrait;

    public function default(): array
    {
        return [
            [
                'applianceId' => 1,
                'groupId' => 1
            ]
        ];
    }
}
