<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

/**
 * Interface SeedInterface
 * @package netzwolke\Factory\DB\Seed\Migration
 */
interface SeedInterface
{
    public function default(): array;
}
