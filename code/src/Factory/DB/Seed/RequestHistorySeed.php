<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

/**
 * Class RequestHistorySeed
 * @package netzwolke\Factory\DB\Seed\Migration\Seed
 */
class RequestHistorySeed implements SeedInterface
{

    /**
     * @param array $seeds
     */
    public function run(array $seeds): void
    {
        // TODO: Implement run() method.
    }

    /**
     * @return array
     */
    public function default(): array
    {
        // TODO: Implement default() method.
    }
}
