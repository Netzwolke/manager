<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Seed;

use Netzwolke\Manager\Factory\DB\SeedTrait;
use Netzwolke\Manager\Model\HttpRequest;

/**
 * Class HttpRequestSeed
 * @package netzwolke\Factory\DB\Seed\Migration\Seed
 */
class HttpRequestSeed extends HttpRequest implements SeedInterface
{
    use SeedTrait;

    /**
     * @return array
     */
    public function default(): array
    {
        return [
            [
                'name' => 'test',
                'url' => "https://api.binance.com",
                'uri' => "/api/v3/ticker/24hr",
                'method' => 'GET',
                'options' => json_encode(array('query' => array('symbol' => 'BTCEUR'))),
                'userId' => 1

            ]
        ];
    }
}
