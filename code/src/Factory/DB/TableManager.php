<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

use Closure;
use Illuminate\Database\Capsule\Manager;

/**
 * Class TableManager
 * @package Netzwolke\Manager\Factory\DB
 */
class TableManager
{
    /**
     * @var array
     */
    private array $migrations;
    /**
     * @var array
     */
    private array $seeds;

    /**
     * TableManager constructor.
     * @param Executor $executor
     * @param Migrator $migrator
     * @param Seeder $seeder
     * @param Manager $manager
     */
    public function __construct(
        private Executor $executor,
        private Migrator $migrator,
        private Seeder $seeder,
        private Manager $manager
    ) {
        $this->loadAllClasses();
    }

    /**
     * @param string $table
     * @param Closure $blueprint
     */
    public function start(string $table, Closure $blueprint)
    {
        $this->manager
            ->getConnection()
            ->getSchemaBuilder()
            ->create($table, $blueprint);
    }

    /**
     * @param string $class
     * @return string
     */
    public function getTable(string $class): string
    {
        return $this->executor->exec($class, 'getTable');
    }

    /**
     * @param string $class
     * @return bool
     */
    public function hasTable(string $class): bool
    {
        return $this->executor->exec($class, 'hasTable');
    }

    /**
     * @param string $table
     * @return bool
     */
    public function exists(string $table): bool
    {
        $class = $this->migrations[$table];
        return $this->hasTable($class);
    }


    /**
     * @param array $classes
     */
    public function up(array $classes = [])
    {
        foreach ($classes as $class) {
            $table = $this->getTable($class);
            if (! $this->hasTable($class)) {
                $this->start(
                    $table,
                    $this->executor->exec($class, 'up')
                );
            }
        }
    }

    /**
     * @param array $classes
     */
    public function down(array $classes = [])
    {
        foreach ($classes as $class) {
            $table = $this->getTable($class);
            $this->manager->getConnection()->getSchemaBuilder()->dropIfExists($table);
        }
    }

    /**
     * @param array $classes
     */
    public function seed(array $classes = [])
    {
        $this->executor->execs($classes, 'run');
    }

    /**
     * @return array
     */
    public function loadAllClasses(): array
    {
        foreach ($this->seeder->loadSeeds() as $seed) {
            $table = $this->getTable($seed);
            $this->seeds[$table] = $seed;
        }

        foreach ($this->migrator->loadMigrations() as $migration) {
            $table = $this->getTable($migration);
            $this->migrations[$table] = $migration;
        }
        return [$this->migrations, $this->seeds];
    }

    /**
     * @return array
     */
    public function loadSeeds(): array
    {
        return $this->seeds;
    }

    /**
     * @return array
     */
    public function loadMigrations(): array
    {
        return $this->migrations;
    }
}
