<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Class Manager
 * @package Netzwolke\Manager\Factory\DB
 */
class Manager
{
    /**
     * @var array
     */
    public array $migrations;
    /**
     * @var array
     */
    public array $tables;
    /**
     * @var array
     */
    public array $seeds;

    /**
     * Manager constructor.
     * @param SeederInterface $seeder
     * @param MigratorInterface $migrator
     * @param Executor $executor
     * @param string $db
     */
    public function __construct(
        public SeederInterface $seeder,
        public MigratorInterface $migrator,
        public Executor $executor,
        public string $db
    ) {
    }

    /**
     * @return bool
     */
    public function DBisEmpty(): bool
    {
        $array = Capsule::schema('base')
            ->getConnection()
            ->select("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$this->db';");
        if (count($array) > 0) {
            return false;
        }
        return true;
    }

    /**
     *
     */
    public function createDB()
    {
        Capsule::schema('base')->getConnection()->statement("CREATE DATABASE IF NOT EXISTS $this->db;");
    }

    /**
     * @param array $classes
     * @param array $names
     * @return array
     */
    public function getClasses(array $classes, array $names): array
    {
        return array_intersect_key($classes, array_flip($names));
    }
}
