<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Model\Appliance;

/**
 * Class ApplianceMigration
 * @package Netzwolke\Manager\Factory\DB\Migration
 */
class ApplianceMigration extends Appliance implements MigrationInterface
{
    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->boolean('disabled')->default(false);
            $table->timestamps();
        };
    }
}
