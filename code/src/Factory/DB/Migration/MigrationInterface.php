<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;

/**
 * Interface MigrationInterface
 * @package netzwolke\Factory\DB\Seed\Migration
 */
interface MigrationInterface
{
    public function up(): Closure;
}
