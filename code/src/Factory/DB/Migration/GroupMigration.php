<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Model\Group;

/**
 * Class GroupMigration
 * @package Netzwolke\Manager\Factory\DB\Migration
 */
class GroupMigration extends Group implements MigrationInterface
{

    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('disabled')->default(false);
            $table->timestamps();
        };
    }
}
