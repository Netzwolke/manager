<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class RequestHistoryMigration
 * @package netzwolke\Factory\DB\Seed\Migration\Migration
 */
class RequestHistoryMigration implements MigrationInterface
{
    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->json('options');
            $table->integer('workerId')->nullable();
            $table->integer('userId');
            $table->timestamps();
        };
    }
}
