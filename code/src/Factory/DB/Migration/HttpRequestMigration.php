<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Model\HttpRequest;

/**
 * Class HttpRequestMigration
 * @package netzwolke\Factory\DB\Seed\Migration\Migration
 */
class HttpRequestMigration extends HttpRequest implements MigrationInterface
{

    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->string('uri');
            $table->string('method');
            $table->json('options');
            $table->integer('workerId')->nullable();
            $table->integer('userId');
            $table->timestamps();
        };
    }
}
