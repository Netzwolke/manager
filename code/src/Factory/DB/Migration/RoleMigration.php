<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Model\Role;

/**
 * Class RoleMigration
 * @package netzwolke\Factory\DB\Seed\Migration\Migration
 */
class RoleMigration extends Role implements MigrationInterface
{
    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('power');
            $table->boolean('disabled')->default(false);
            $table->timestamps();
        };
    }
}
