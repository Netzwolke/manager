<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Model\UserLog;

/**
 * Class UserMigration
 * @package netzwolke\Factory\DB\Seed\Migration\Migration
 */
class UserLogMigration extends UserLog implements MigrationInterface
{
    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->integer('signedIn');
            $table->string('ip');
            $table->integer('attempts');
            $table->text('userAgent');
            $table->string('logId');
            $table->timestamps();
        };
    }
}
