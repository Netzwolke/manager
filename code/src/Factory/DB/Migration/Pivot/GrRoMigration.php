<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration\Pivot;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Factory\DB\Migration\MigrationInterface;
use Netzwolke\Manager\Model\Pivot\GrRo;

/**
 * Class ApplianceMigration
 * @package Netzwolke\Manager\Factory\DB\Migration
 */
class GrRoMigration extends GrRo implements MigrationInterface
{
    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groupId');
            $table->integer('roleId');
            $table->timestamps();
        };
    }
}
