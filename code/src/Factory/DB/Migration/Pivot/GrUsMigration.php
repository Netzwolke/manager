<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB\Migration\Pivot;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Netzwolke\Manager\Factory\DB\Migration\MigrationInterface;
use Netzwolke\Manager\Model\Pivot\GrUs;

/**
 * Class ApplianceMigration
 * @package Netzwolke\Manager\Factory\DB\Migration
 */
class GrUsMigration extends GrUs implements MigrationInterface
{
    /**
     * @return Closure
     */
    public function up(): Closure
    {
        return function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groupId');
            $table->integer('userId');
            $table->timestamps();
        };
    }
}
