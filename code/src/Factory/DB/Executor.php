<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

/**
 * Class Executor
 * @package Netzwolke\Manager\Factory\DB
 */
class Executor implements ExecutorInterface
{
    /**
     * @param array $classes
     * @param string $function
     * @param array $params
     */
    public function execs(array $classes, string $function, array $params = []): void
    {
        foreach ($classes as $class) {
            $obj = new $class();
            ([$obj, $function])(...$params);
        }
    }

    /**
     * @param string $class
     * @param string $function
     * @param array $params
     * @return mixed
     */
    public function exec(string $class, string $function, array $params = []): mixed
    {
        $obj = new $class();
        return ([$obj, $function])(...$params);
    }

    public function getTableNames()
    {
    }
}
