<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

use Netzwolke\Manager\Factory\DB\Migration\ApplianceMigration;
use Netzwolke\Manager\Factory\DB\Migration\GroupMigration;
use Netzwolke\Manager\Factory\DB\Migration\HttpRequestMigration;
use Netzwolke\Manager\Factory\DB\Migration\Pivot\ApGrMigration;
use Netzwolke\Manager\Factory\DB\Migration\Pivot\GrRoMigration;
use Netzwolke\Manager\Factory\DB\Migration\Pivot\GrUsMigration;
use Netzwolke\Manager\Factory\DB\Migration\Pivot\UsRoMigration;
use Netzwolke\Manager\Factory\DB\Migration\RoleMigration;
use Netzwolke\Manager\Factory\DB\Migration\UserLogMigration;
use Netzwolke\Manager\Factory\DB\Migration\UserMigration;

/**
 * Class Migrator
 * @package netzwolke\Factory\DB\Seed\Migration
 */
class Migrator implements MigratorInterface
{
    public function __construct(
        private ExecutorInterface $executor
    ) {
    }

    /**
     * @return string[]
     */
    public function loadMigrations(): array
    {
        return [
            UserMigration::class,
            RoleMigration::class,
            UserLogMigration::class,
            HttpRequestMigration::class,
            ApplianceMigration::class,
            GroupMigration::class,
            ApGrMigration::class,
            GrRoMigration::class,
            UsRoMigration::class,
            GrUsMigration::class,
        ];
    }
}
