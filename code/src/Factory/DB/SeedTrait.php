<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\DB;

use Netzwolke\Manager\Model\BaseModel;

/**
 * Class DatabaseInterface
 * @package netzwolke\Factory\DB\Seed\Migration
 */
trait SeedTrait
{
    public function run()
    {
        $default = $this->default();
        if (!is_subclass_of($this, BaseModel::class)) {
            return;
        }
        foreach ($default as $item) {
            self::create($item);
        }
    }
}
