<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory;

use Slim\App;

/**
 * Class ModelFactory
 * @package netzwolke\Factory
 */
class ModelFactory
{
    public function __construct(App $app)
    {
        $this->createEloquent($app);
    }

    /**
     * @param App $app
     */
    public function createEloquent(App $app)
    {
        $capsule = $app->getContainer()->get('Capsule');
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}
