<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory;

use Closure;
use DI\Bridge\Slim\Bridge;
use DI\Container;
use DI\ContainerBuilder;
use Exception;
use GuzzleHttp\Client;
use Netzwolke\Logger\DBManager;
use Netzwolke\Logger\ErrorHandler;
use Netzwolke\Logger\Logger;
use Netzwolke\Manager\Factory\DB\Executor;
use Netzwolke\Manager\Factory\DB\ExecutorInterface;
use Netzwolke\Manager\Factory\DB\Migrator;
use Netzwolke\Manager\Factory\DB\MigratorInterface;
use Netzwolke\Manager\Factory\DB\Seeder;
use Netzwolke\Manager\Factory\DB\SeederInterface;
use Netzwolke\Manager\Factory\DB\TableManager;
use Netzwolke\Manager\Factory\Middleware\AuthMiddleware;
use Netzwolke\Manager\Factory\Middleware\DebugMiddleware;
use Netzwolke\Manager\Factory\Middleware\MigrationMiddleware;
use Netzwolke\Manager\Factory\Middleware\PaginationMiddleware;
use Netzwolke\Manager\Factory\Middleware\SessionMiddleware;
use Netzwolke\Manager\Factory\Twig\Functions\CoreTwigFunction;
use Netzwolke\Manager\Factory\Twig\Functions\FormTwigFunction;
use Netzwolke\Manager\Factory\Twig\Functions\PaginateTwigFunction;
use Netzwolke\Manager\Factory\Twig\TwigExtension;
use Netzwolke\Manager\Factory\Twig\TwigRuntimeLoader;
use Netzwolke\Manager\Model\Resources\ManagerExtension;
use Netzwolke\Manager\Model\Resources\Pagination;
use Netzwolke\Manager\Model\Resources\PaginationInterface;
use Netzwolke\Manager\Resources\Output\Message;
use Netzwolke\Manager\Resources\Output\MessageInterface;
use Netzwolke\Manager\Resources\Output\Messenger;
use Netzwolke\Manager\Resources\Output\MessengerInterface;
use Netzwolke\Manager\Resources\WebStorage\Auth;
use Netzwolke\Manager\Resources\WebStorage\Session;
use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\AuthWrapperInterface;
use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\SessionWrapperInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Middleware\ErrorMiddleware;
use Slim\Middleware\MethodOverrideMiddleware;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use function DI\create;
use function DI\factory;
use function DI\get;

/**
 * Class DependencyInjectionFactory
 * @package netzwolke\Factory
 */
class DependencyInjectionFactory
{
    /**
     * @var Container
     */
    private Container $container;
    /**
     * @var mixed
     */
    private array $config;

    /**
     * DependencyInjectionFactory constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->config = include_once __DIR__ . '/Factory.config.php';
        $this->createBridge();
    }

    /**
     * @return App
     */
    public function getApp(): App
    {
        return Bridge::create($this->container);
    }

    /**
     * @throws Exception
     */
    public function createBridge()
    {
        $builder = new ContainerBuilder();
        $builder->addDefinitions(
            $this->setFrontendDefinitions(),
            $this->setMiddlewareDefinitions(),
            $this->setResourcesDefinitions(),
            $this->setAppDefinitions(),
            $this->setModelDefinitions()
        );
        $this->container = $builder->build();
    }

    /**
     * @return Closure[]
     */
    private function setAppDefinitions(): array
    {
        return [
            Executor::class => create(),
            ExecutorInterface::class => get(Executor::class),
            RouteParserInterface::class => function (RouteCollectorInterface $collector) {
                return $collector->getRouteParser();
            },
            Seeder::class => create()->constructor(
                get(Executor::class)
            ),
            SeederInterface::class => get(Seeder::class),
            Migrator::class => create()->constructor(
                get(Executor::class)
            ),
            MigratorInterface::class => get(Migrator::class),
            DB\Manager::class => create()->constructor(
                get(SeederInterface::class),
                get(MigratorInterface::class),
                get(Executor::class),
                $this->config['model']['database']
            ),
            'remoteClient' => factory(function () {
                return new Client([
                    'base_uri' => 'https://manager.netzwolke.com/',
                    'http_errors' => false
                ]);
            }),
            'localClient' => factory(function () {
                return new Client([
                    'base_uri' => 'http://nginx',
                    'http_errors' => false
                ]);
            }),

        ];
    }

    /**
     * @return array
     */
    private function setResourcesDefinitions(): array
    {
        return [
            Session::class => create(),
            SessionWrapperInterface::class => get(Session::class),
            Logger::class => create()->constructor(
                get(DBManager::class)
            ),
            ErrorHandler::class => create()->constructor(get(Logger::class)),
            LoggerInterface::class => get(Logger::class),
            DBManager::class => create()->constructor(get('Capsule')),
            Auth::class => create()->constructor(
                get(SessionWrapperInterface::class),
                get(MessengerInterface::class),
                get('remoteClient'),
                get('localClient'),
                $this->config['env'],
                $this->config['key'],
                $this->config['version'],
                get(LoggerInterface::class)
            ),
            AuthWrapperInterface::class => get(Auth::class),

                Messenger::class => create()->constructor(
                    get(SessionWrapperInterface::class),
                    get(MessageInterface::class)
                ),
                'Messenger' => get(Messenger::class),
                MessengerInterface::class => get('Messenger'),
                Message::class => create(),
                MessageInterface::class => get(Message::class),


            ];
    }

    /**
     * @return array
     */
    public function setModelDefinitions(): array
    {
        return [
            ManagerExtension::class => factory(function ($database, PaginationInterface $pagination) {
                $capsule = new ManagerExtension($pagination);
                $capsule->addConnection($database);
                $database['database'] = '';
                $capsule->addConnection($database, 'base');
                $database['database'] = 'error_schema';
                $capsule->addConnection($database, 'error');
                return $capsule;
            })
                ->parameter('database', $this->config['model']),
            'Capsule' => get(ManagerExtension::class),
            Pagination::class => create()->constructor(
                get(RouteParserInterface::class)
            ),
            PaginationInterface::class => get(Pagination::class),
            TableManager::class => create()->constructor(
                get(Executor::class),
                get(Migrator::class),
                get(Seeder::class),
                get(ManagerExtension::class)
            )
        ];
    }

    /**
     * @return array
     */
    private function setMiddlewareDefinitions(): array
    {
        return [
            DebugMiddleware::class => create()->constructor(
                get(MessengerInterface::class)
            ),
            MigrationMiddleware::class => create()->constructor(
                get(DB\Manager::class),
                get(TableManager::class)
            ),
            AuthMiddleware::class => create()->constructor(
                get(AuthWrapperInterface::class),
                get(RouteParserInterface::class)
            ),
            SessionMiddleware::class => create()->constructor(
                get(SessionWrapperInterface::class)
            ),
            MethodOverrideMiddleware::class => create(),
            TwigMiddleware::class => factory(function (
                Twig $twig,
                RouteParserInterface $routeParser
            ) {
                return new TwigMiddleware($twig, $routeParser, '', Twig::class);
            }),
            ErrorMiddleware::class => factory(
                function (
                    CallableResolverInterface $callableResolver,
                    ResponseFactoryInterface $factory,
                    LoggerInterface $logger,
                    ErrorHandler $errorHandler
                ) {
                    $errorMiddleware =  new ErrorMiddleware($callableResolver, $factory, true, true, true, $logger);
                    $errorMiddleware->setDefaultErrorHandler($errorHandler);
                    return $errorMiddleware;
                }
            ),
            PaginationMiddleware::class => create()->constructor(
                get(PaginationInterface::class)
            )
        ];
    }

    /**
     * @return array
     */
    private function setFrontendDefinitions(): array
    {
        return [
            TwigRuntimeLoader::class => create()->constructor(
                get(ContainerInterface::class)
            ),
            Twig::class => factory(function (
                TwigExtension $extension,
                TwigRuntimeLoader $loader,
                $path,
                $settings
            ) {

                $twig = Twig::create($path, $settings);
                $twig->addRuntimeLoader($loader);
                $twig->addExtension($extension);
                return $twig;
            })
                ->parameter('path', $this->config['view']['path'])
                ->parameter('settings', $this->config['view']['settings']),
            CoreTwigFunction::class => create()->constructor(
                get(MessengerInterface::class),
                get(AuthWrapperInterface::class),
                get(RouteCollectorInterface::class)
            ),
            FormTwigFunction::class => create(),
            PaginateTwigFunction::class => create()->constructor(
                get(PaginationInterface::class)
            )
        ];
    }
}
