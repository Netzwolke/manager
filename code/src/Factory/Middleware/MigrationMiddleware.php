<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Middleware;

use Netzwolke\Manager\Factory\DB\Manager;
use Netzwolke\Manager\Factory\DB\TableManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class MigrationMiddleware
 * @package Netzwolke\Manager\Factory\Middleware
 */
class MigrationMiddleware implements MiddlewareInterface
{
    public function __construct(
        private Manager $manager,
        private TableManager $tableManager
    ) {
    }

    /**
     * @inheritDoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->manager->DBisEmpty()) {
            $this->manager->createDB();
            $this->tableManager->up($this->tableManager->loadMigrations());
            $this->tableManager->seed($this->tableManager->loadSeeds());
        }
        return $handler->handle($request);
    }
}
