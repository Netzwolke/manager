<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Middleware;

use Netzwolke\Manager\Resources\Output\MessengerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class DebugMiddleware
 * @package netzwolke\Factory\Middleware
 */
class DebugMiddleware implements MiddlewareInterface
{
    /**
     * @var MessengerInterface
     */
    private MessengerInterface $messenger;

    public function __construct(MessengerInterface $messenger)
    {
        $this->messenger = $messenger;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $message = json_encode($request->getServerParams());
        //$this->messenger->addSuccess($message);
        return $handler->handle($request);
    }
}
