<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Middleware;

use Netzwolke\Manager\Model\Resources\PaginationInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteContext;

/**
 * Class SessionMiddleware
 * @package netzwolke\Factory\Middleware
 */
class PaginationMiddleware implements MiddlewareInterface
{
    public function __construct(
        private PaginationInterface $pagination
    ) {
    }



    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (null !== ($page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE))) {
            if ($page) {
                $this->pagination->setCurrentPage($page);
            }
        }
        $route = RouteContext::fromRequest($request)->getRoute();
        $this->pagination->setRoute($route);
        $this->pagination->setQueryParams($request->getQueryParams());

        return $handler->handle($request);
    }
}
