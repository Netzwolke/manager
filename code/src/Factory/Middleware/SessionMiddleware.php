<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Middleware;

use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\SessionWrapperInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteContext;

/**
 * Class SessionMiddleware
 * @package netzwolke\Factory\Middleware
 */
class SessionMiddleware implements MiddlewareInterface
{
    /**
     * @var SessionWrapperInterface
     */
    private SessionWrapperInterface $session;

    public function __construct(SessionWrapperInterface $session)
    {
        $this->session = $session;
    }



    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $context = RouteContext::fromRequest($request);
        $parser = $context->getRouteParser();
        $route = $context->getRoute();

        //start Session
        $this->session->startSession();
        //save the current route to the browsing history in the session
        if ($request->getMethod() === "GET") {
            if ($parser->urlFor($route->getName(), $route->getArguments())) {
                $this->session->setHistory($request->getUri()->getPath());
            }
        }

        return $handler->handle($request);
    }
}
