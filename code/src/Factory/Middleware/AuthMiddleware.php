<?php

declare(strict_types=1);

namespace Netzwolke\Manager\Factory\Middleware;

use Netzwolke\Manager\Resources\WebStorage\WrapperInterface\AuthWrapperInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Psr7\Response;

/**
 * Class AuthMiddleware
 * @package netzwolke\Factory\Middleware
 */
class AuthMiddleware implements MiddlewareInterface
{


    /**
     * @var AuthWrapperInterface
     */
    private AuthWrapperInterface $auth;
    /**
     * @var RouteParserInterface
     */
    private RouteParserInterface $parser;


    public function __construct(AuthWrapperInterface $auth, RouteParserInterface $parser)
    {
        $this->parser = $parser;
        $this->auth = $auth;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $code = $this->auth->isApp($request);
        $response = new Response();
        $response->withStatus($code);

        if ($this->auth->isAuth()) {
            $this->auth-> refreshUserLog();
            return $handler->handle($request);
        }
        if ($code < 201) {
            return $handler->handle($request);
        }
        if ($code === 403) {
            $response->getBody()->write(json_encode([
                'Status' => 'Access denied',
                'reason' => 'Wrong or missing authentication'
            ]));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($code);
        }
        return $response->withStatus(403)->withHeader('location', $this->parser->urlFor('auth.authenticate'));
    }
}
