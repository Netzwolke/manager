<?php

declare(strict_types=1);

use Netzwolke\Manager\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$factory = new AppFactory();
$app = $factory->createApp();
$app->run();
