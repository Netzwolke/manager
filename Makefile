ifeq (${BUILD_NUMBER},)
	VERSION := "latest"
else
	VERSION := ${BUILD_NUMBER}
endif

ifeq (${DEPLOYMENT_ENVIRONMENT},)
	ENV:= "dev"
else
	ENV:= ${DEPLOYMENT_ENVIRONMENT}
endif

CONFIG:= ".env."$(ENV)
VHOST = manager
PHP_BIN = php
COMPOSER = /usr/local/bin/composer
DOCKER = docker-compose
WORKER = $(DOCKER) -f docker/compose/docker-compose-worker.yml
DEV = $(DOCKER) -f docker/compose/docker-compose-dev.yml
PRD = $(DOCKER) -f docker/compose/docker-compose-prd.yml
REPO = netzwolke/netzwolke
CLI = $(DOCKER) -f docker/compose/docker-compose-cli.yml run --rm --no-deps -w /var/www/html/ php_cli

.PHONY: push
push: build_dev
	docker image push $(REPO):$(VHOST)-php_fpm-$(VERSION)
	docker image push $(REPO):$(VHOST)-php_fpm-latest
	docker image push $(REPO):$(VHOST)-nginx-$(VERSION)
	docker image push $(REPO):$(VHOST)-nginx-latest

.PHONY: build_config
build_config: build_config_stage1 build_config_stage2

build_config_stage2:
	make ansible_decrypt file=config/.env

build_config_stage1:
	cp -f config/$(CONFIG) ./config/.env

.PHONY: save_config
save_config: save_config_stage1 save_config_stage2
save_config_stage1:
	make ansible_encrypt file=config/.env

save_config_stage2:
	cp -f ./config/.env config/$(CONFIG)
	rm ./config/.env

.PHONY: build_dev
build_dev: create_tag_file
	docker build -f docker/php_fpm/Dockerfile . \
		--build-arg NUMBER=$(VERSION) \
		-t $(REPO):$(VHOST)-php_fpm-$(VERSION) \
		-t $(REPO):$(VHOST)-php_fpm-latest
	docker build -f docker/nginx/Dockerfile . \
		-t $(REPO):$(VHOST)-nginx-$(VERSION) \
		-t $(REPO):$(VHOST)-nginx-latest

.PHONY: create_tag_file
create_tag_file:
	echo $(VERSION) > image_tag.txt

.PHONY: create_artifact
create_artifact:
	tar -cvzf release.tar.gz Makefile ansible/* image_tag.txt docker/compose/* config/.env*

.PHONY: create_secret
create_secret:
	echo "${ANSIBLE_SECRET}" > $$(pwd)/secret.sh


.PHONY: build_ansible
build_ansible:
	ansible-playbook \
	$$(pwd)/ansible/build-plan.yml -vv

.PHONY: deploy
deploy: create_secret
	ansible-playbook \
	-e tag=$$(cat image_tag.txt) \
	-e env=$(ENV) \
	-i $$(pwd)/ansible/$(ENV).ini $$(pwd)/ansible/deploy-plan.yml --vault-password-file $$(pwd)/secret.sh -vv

.PHONY: pull-worker
pull-worker:
		$(WORKER) pull

.PHONY: pull-development
pull-development: pull_dev development

pull_dev:
		$(DEV) pull

.PHONY: development
development: build_config
		$(DEV) up -d

.PHONY: development-stop
development-stop: save_config
	$(DEV) down --remove-orphans


.PHONY: production
production:
	$(PRD) pull
	$(PRD) up -d


.PHONY: production-stop
production-stop:
	$(PRD) down

.PHONY: install
install:
	$(CLI) $(PHP_BIN) -d memory_limit=-1 $(COMPOSER) install

.PHONY: update
update:
	$(CLI) $(PHP_BIN) -d memory_limit=-1 $(COMPOSER) update

.PHONY: docker_sh
docker_sh:
	$(CLI) sh

.PHONY: ansible_sh
ansible_sh:
	$(WORKER) run --rm --no-deps worker sh

.PHONY:  ansible_encrypt
ansible_encrypt:
	docker run --rm -it \
		--name=ansible \
		-v $$(pwd):/app/target \
		-e File=$(file) \
		-e ANSIBLE_SECRET=$(pass) \
		-w /app/target \
		$(REPO):worker-latest \
		sh -c "sh ../encrypt.sh"

.PHONY: ansible_decrypt
ansible_decrypt:
	docker run --rm -it \
		--name=ansible \
		-v $$(pwd):/app/target \
		-e File=$(file) \
		-e ANSIBLE_SECRET=$(pass) \
		-w /app/target \
		$(REPO):worker-latest \
		sh -c "sh ../decrypt.sh"

.PHONY: ansible_test
ansible_test:
	docker run --rm -it \
		--name=ansible \
		-v $$(pwd):/app/target \
		-w /app/target \
		$(REPO):worker-latest \
		sh -c "ansible-playbook \
               	test.yml"
