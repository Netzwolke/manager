# Welcome to this Manager Application

### first steps:

- start dev environment and decrypt config
    - `make development pass=secret`
- stop dev enviornment and encrypt config
    - `make development-stop pass=secret`


### Docs:

- Slim v4 [Slimframework](https://www.slimframework.com/docs/v4/start/installation.html)
- Dependency Injection with [PHP-DI](https://php-di.org/doc/php-definitions.html)
- Eloquent [laravel/Eloquent_ORM](https://laravel.com/docs/8.x/eloquent)
- Twig [symfony/Twig/docs](https://twig.symfony.com/doc/3.x/)





